FROM termux/package-builder

USER root:root

RUN chown -R builder:builder /home/builder/termux-packages && \
    git clone https://github.com/termux/termux-packages.git . && \
    rm -rf packages

COPY . /home/builder/termux-packages

USER builder:builder
