TERMUX_PKG_HOMEPAGE=https://github.com/udp/json-parser
TERMUX_PKG_DESCRIPTION="Very low footprint JSON parser written in portable ANSI C"
TERMUX_PKG_LICENSE="BSD-2-Clause"
TERMUX_PKG_MAINTAINER="Lucas Ramage @oxr463"
TERMUX_PKG_VERSION=1.1.0
TERMUX_PKG_SRCURL=https://github.com/udp/json-parser/archive/v1.1.0.tar.gz
TERMUX_PKG_SHA256=5c278793269dbbf98d5f1592c797234581df69088d2838a14154b4af52ebd133
