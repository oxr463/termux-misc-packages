TERMUX_PKG_HOMEPAGE=https://gitlab.com/jobol/mustach
TERMUX_PKG_DESCRIPTION="C implementation of mustache templating"
TERMUX_PKG_LICENSE="Apache-2.0"
TERMUX_PKG_MAINTAINER="Lucas Ramage @oxr463"
TERMUX_PKG_VERSION=0.99
TERMUX_PKG_SRCURL=https://gitlab.com/jobol/mustach/-/archive/0.99/mustach-0.99.tar.bz2
TERMUX_PKG_SHA256=b8123f2e8a81e035ca66dff85d0625979fb4dca2254f1bef123b67a77f95a037
