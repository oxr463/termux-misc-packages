TERMUX_PKG_HOMEPAGE=https://gitlab.com/oxr463/check-generator
TERMUX_PKG_DESCRIPTION="Generate unit tests for c projects via check"
TERMUX_PKG_LICENSE="GPL-2.0-or-later"
TERMUX_PKG_MAINTAINER="Lucas Ramage @oxr463"
TERMUX_PKG_VERSION=0.1.0
TERMUX_PKG_SRCURL=https://gitlab.com/oxr463/check-generator/-/archive/v0.1.0/check-generator-v0.1.0.tar.bz2
TERMUX_PKG_SHA256=5309e7b7959d9de0130754cc7a5906440dfc0902375329fb9cafcad5a9e997d3
