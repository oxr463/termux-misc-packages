TERMUX_PKG_HOMEPAGE=https://github.com/rebootuser/LinEnum
TERMUX_PKG_DESCRIPTION="Scripted Local Linux Enumeration & Privilege Escalation Checks"
TERMUX_PKG_LICENSE="MIT"
TERMUX_PKG_MAINTAINER="Lucas Ramage @oxr463"
TERMUX_PKG_VERSION=0.982
TERMUX_PKG_SRCURL=https://github.com/rebootuser/LinEnum/archive/master.tar.gz
TERMUX_PKG_SHA256=bd5dffb5df9db21caa7cdc4f099a8ca8a6b60f963c31170cba87ddc12c4b5003
